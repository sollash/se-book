## Introduction
In the Shumaker's world software engineering is what we would like to call a natural tool in our belt.  We have had a long line of engineer/math gurus in our past and naturally we have evolved into becoming IT nerds.  Ever since I was a child I was raised with computers, I could build them before I could speak!  The thing that intrigues me the most about SE is the fact that you never know it all and it's a challenge to keep up.  This is what makes it fun to be in the IT world.

For me personally, to define software engineering, it is about making software products including online software applications, software tools, and libraries with a set of requirements given for the project.  My definition is of only one person, let's look at some other opinions on the matter...

**[Wikipedia]** (The most trusted source) - *"A software engineer is a person who applies the principles of software engineering to the design, development, maintenance, testing, and evaluation of the software and systems that make computers or anything containing software work."*

This definition is pretty spot on but extremely simple.  If I were to explain software engineering to someone I would be ok to say this, or tell them to Google it and look at the first result like I did.  As I kept reading the article, the best line I found was *"software engineers are distinguished from people who specialize in only one role* [or many] * because they take part in the design as well as the programming of the project"* which to me would be an even more perfect definition of a software engineer.  Being an expert in a technology to help bring something to life, really speaks to me.

**Kiri** (My Fiancee, an animal scientist) - *"A software engineer is someone who works with computer code to repair, maintain and create new computer programs. They have an intimate knowledge of a wide variety of programming languages, and can decipher which of these languages would be most appropriate for the project at hand. Software engineers are adept at manipulating code to achieve the desired results, and possess a strong background in mathematics and a keen eye for patterns. I am not someone who is computer savvy, and so I think what computer software engineers do is incredible, taking lines of symbols, letters and numbers and turning it into applications that can sort through an entire site's inventory to find a product, suggest a product based on your search criteria, or even correct your spelling! "*

You can tell from this person's definition they are extremely intelligent and very beauti... ok she walked away.  This definition, I can agree with it, but I would have to say it has been heavily influenced from all the projects I've explained to her throughout my IT career.  I agree that we use technology to repair, maintain, and create new applications.  This is what we have been trained to do!  The only few things I disagree with is the mathematics, keen eye for patterns, and symbols/letter/numbers turning into a program.  Software engineering isn't about specific languages or technologies, it's the idea of all of it together.  You could classify building a physical calculator with cogs/gears as software engineering because it's performing computations!

**Tylan (Scrum Master @ Kroger)**
*"Software Engineering: also known as Software Development, which includes the creation of applications and/or programs that allow users to accomplish desired tasks with electronic devices. "*

I agree that this is a definition of a software developer who focuses on the specifics to make a whole application.  Software engineering to me is more of the project and managing of a whole set of these developed applications.




## Glossary

**No	Silver	Bullet** - This means there is no simple single way of doing something (a cure all scenario).  In technology there is no single way in technology nor management that produces the maximum magnitude in productivity, reliability, or simplicity.

**Brooks's law** - Adding human resources to a late software project makes it later.  This means that trying to add 'manpower' to a project only adds to its complexity and won't help you meet your deadlines.  You'll have more people to get on the same page now!

**Moore's Law** - The number of transistors in an integrated circuit doubles every two years, technology advancements.

**Perry's law** - Average programmers are dumb

**Parnas'** - Incompetent programmers are the most often-overlooked risk in software engineering.

**KISS** - Keep it simple and stupid